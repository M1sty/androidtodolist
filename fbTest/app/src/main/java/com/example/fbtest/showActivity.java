package com.example.fbtest;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import   androidx.appcompat.app.AppCompatActivity;
public class showActivity extends AppCompatActivity {
    private TextView tvName,tvSecondName;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //получаем экран лейаута
        setContentView(R.layout.showlayot);
        init();
        getIntentMain();
    }
    private void init() {
        tvName = findViewById(R.id.tvName);
        tvSecondName = findViewById(R.id.tvSecondName);
    }
    private void getIntentMain () {
        Intent i = getIntent();
        if (i != null) {
            tvName.setText(i.getStringExtra(Constant.USER_NAME));
            tvSecondName.setText(i.getStringExtra(Constant.USER_SEC_NAME));
        }
    }

}
