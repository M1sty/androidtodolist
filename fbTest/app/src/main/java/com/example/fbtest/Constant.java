package com.example.fbtest;

public class Constant {
    //Класс в котором будет удобно передавать значения
    //public - общедоступные, final - итоговые( не буду изменяться ) static - можно получать в любом месте
    public static  final String USER_NAME = "user name";
    public static  final String USER_SEC_NAME = "user sec_name";
}
