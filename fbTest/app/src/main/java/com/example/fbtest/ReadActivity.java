package com.example.fbtest;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
// Добавить ап компат
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
//затем на рид активити альт ентер add class to manifest и дописать строчку extends AppCompatActivity
public class ReadActivity extends AppCompatActivity {
    //создаём экран и на нем листвью, затем тут объявляем
    private ListView listView;
    //В адаптер можно передавать только лист
    //а
    private ArrayAdapter<String> adapter;
    //Список передаём в адаптер
    //принимает только 1 значение
    private List<String> listData;
    //Лист который принимает целый класс значение
    private List<User> listTemp;
    private DatabaseReference mDataBase;
    private String User_key = "User";
    //добавлявем oncreate
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.readlayout);
        init();
        getDataFromDB();
        setOnClickItem();
    }
    private void init () {
        listView = findViewById(R.id.listView);
        listData = new ArrayList<>();
        //объявление листа
        listTemp = new ArrayList<>();
        //адаптер который принимает только 1 значение
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,listData);
        listView.setAdapter(adapter);
        mDataBase = FirebaseDatabase.getInstance().getReference(User_key);
    }
    //функция которая будет считывать с бд данные
    private void getDataFromDB () {
        ValueEventListener vListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (listData.size() > 0) {
                    listData.clear();
                }
                if (listTemp.size() > 0) {
                    listTemp.clear();
                }
                //сюда приходят данные
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    //Указываем что хотим получить из бд юзера
                    User user = ds.getValue(User.class);
                    //проверка является ли юзер пустой если он пустой, добавления не будет
                    assert user != null;
                    //Добавляет в лист только имя
                    listData.add(user.name);
                    //Добавляет в лист переменные из всего класса
                    listTemp.add(user);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        //добавление слушателя к бд
        mDataBase.addValueEventListener(vListener);
    }
    // функция которая будет при нажатии на итем открывать экран с информацией
    private void setOnClickItem () {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = listTemp.get(position);
                Intent i = new Intent(ReadActivity.this,showActivity.class);
                //Передаем данные на экран
                i.putExtra(Constant.USER_NAME,user.name);
                i.putExtra(Constant.USER_SEC_NAME,user.secondName);
                startActivity(i);
            }
        });
    }
}
