    package com.example.fbtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

    public class MainActivity extends AppCompatActivity {
    private EditText firstname, secondName;
    private DatabaseReference mDataBase;
    private String User_key = "User";
    //Выше мы объявляем переменные
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Добавляем наши переменные в код методом init
        init();
    }
    //Метод, в котором мы инициализируем наши элементы
    public void init() {
        firstname = findViewById(R.id.name);
        secondName = findViewById(R.id.secondName);
        mDataBase = FirebaseDatabase.getInstance().getReference(User_key);
        //мДатаБасе это переменная, которая будет создавать массив, если его нет и записывать
        //в массив значения

    }

        public void onClickSave(View view) {
            String id = mDataBase.getKey();
            String name = firstname.getText().toString();
            String sName = secondName.getText().toString();
            //В конструктор юзер мы записываем данные, а затем передаём в базу данных
            User user = new User(id,name,sName);
            /*
            * User user - это конструктор класса, который мы создали
            * */
            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(sName)) {
                mDataBase.push().setValue(user);
                //Если условие выполняется, то идёт запись в базу данных
                Toast.makeText(this, "Всё успешно брат", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Пустое поле брат", Toast.LENGTH_SHORT).show();
            }

        }

        public void onClickRead(View view) {
            Intent i = new Intent(MainActivity.this,ReadActivity.class);
            startActivity(i);
        }
    }